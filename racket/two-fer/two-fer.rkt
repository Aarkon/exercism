#lang racket

(provide two-fer)

(define two-fer
  (lambda ([ name "you" ])
    (string-append "One for " name ", one for me.")))
