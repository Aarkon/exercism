#lang racket

(provide square total)

(define (square a-square)
  (expt 2 (- a-square 1)))

(define (total)
  (sum (map square (inclusive-range 1 64))))

(define (sum lst)
 (foldr + 0 lst))
