defmodule KitchenCalculator do
  def get_volume({_, volume}) do
    volume
  end

  def to_milliliter({:cup, amount}), do: {:milliliter, amount * 240}
  def to_milliliter({:fluid_ounce, amount}), do: {:milliliter, amount * 30}
  def to_milliliter({:teaspoon, amount}), do: {:milliliter, amount * 5}
  def to_milliliter({:tablespoon, amount}), do: {:milliliter, amount * 15}
  def to_milliliter({_, amount}), do: {:milliliter, amount}

  def from_milliliter({_, amount}, :cup), do: {:cup, amount / 240}
  def from_milliliter({_, amount}, :fluid_ounce), do: {:fluid_ounce, amount / 30}
  def from_milliliter({_, amount}, :teaspoon), do: {:teaspoon, amount / 5}
  def from_milliliter({_, amount}, :tablespoon), do: {:tablespoon, amount / 15}
  def from_milliliter({_, amount}, _), do: {:milliliter, amount}

  def convert(volume_pair, unit) do
    volume_pair |> to_milliliter() |> from_milliliter(unit)
  end
end
