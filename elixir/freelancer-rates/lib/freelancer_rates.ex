defmodule FreelancerRates do
  def daily_rate(hourly_rate) do
    hourly_rate * 8.0
  end

  def apply_discount(before_discount, discount) do
    before_discount * (100 - discount) / 100
  end

  def monthly_rate(hourly_rate, discount) do
    daily = daily_rate(hourly_rate) * 22
    discounted = apply_discount(daily, discount)
    ceil(discounted)
  end

  def days_in_budget(budget, hourly_rate, discount) do
    without_discount = budget / daily_rate(hourly_rate)
    before_rounding = apply_discount(without_discount, -discount)
    trunc(before_rounding * 10) / 10
  end
end
