defmodule BirdCount do
  def today([]), do: nil
  def today([head | _]), do: head

  def increment_day_count([]), do: [1]
  def increment_day_count([head | tail]), do: [head + 1 | tail]

  def has_day_without_birds?(list), do: 0 in list

  def total(list, acc \\ 0)
  def total([], acc), do: acc
  def total([head | tail], acc), do: total(tail, acc + head)

  def busy_days(list, acc \\ 0)
  def busy_days([], acc), do: acc

  def busy_days([head | tail], acc) do
    if head >= 5 do
      busy_days(tail, acc + 1)
    else
      busy_days(tail, acc)
    end
  end
end
