defmodule HighScore do
  @default_score 0

  def new(), do: %{}

  def add_player(scores, name, score \\ @default_score),
    do: Map.update(scores, name, score, fn _ -> 0 end)

  def remove_player(scores, name), do: Map.delete(scores, name)

  def reset_score(scores, name), do: remove_player(scores, name) |> add_player(name)

  def update_score(scores, name, score),
    do: Map.update(scores, name, score, fn old -> old + score end)

  def get_players(scores), do: Map.keys(scores)
end
