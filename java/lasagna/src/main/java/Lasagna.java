public class Lasagna {
    public int expectedMinutesInOven() {
        return 40;
    }

    public int remainingMinutesInOven(int minutesAlreadyInOven) {
        return expectedMinutesInOven() - minutesAlreadyInOven;
    }

    public int preparationTimeInMinutes(int layers) {
        return layers * 2;
    }

    public int totalTimeInMinutes(int layers, int minutesAlreadyInOven) {
        return minutesAlreadyInOven + preparationTimeInMinutes(layers);
    }
}
