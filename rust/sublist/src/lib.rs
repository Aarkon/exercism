#[derive(Debug, PartialEq, Eq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    let every_first_in_second = every_a_in_b(first_list, second_list);
    let every_second_in_first = every_a_in_b(second_list, first_list);

    if every_second_in_first && every_first_in_second {
        Comparison::Equal
    } else if every_first_in_second {
        Comparison::Sublist
    } else if every_second_in_first {
        Comparison::Superlist
    } else {
        Comparison::Unequal
    }
}

fn every_a_in_b<T: PartialEq>(a: &[T], b: &[T]) -> bool {
    a.iter().fold(true, |acc, x| {
        if !acc {
            acc
        } else {
            b.iter().find(|&y| x == y).is_some()
        }
    })
}

#[cfg(test)]
mod sublist_tests {
    use crate::every_a_in_b;

    #[test]
    fn test_every_a_in_b() {
        assert!(every_a_in_b(&[1, 2, 3], &[1, 2, 3]));
    }

    #[test]
    fn test_every_a_in_b_sublist() {
        assert!(every_a_in_b(&[1, 3], &[1, 2, 3]));
    }

    #[test]
    fn test_every_a_in_b_shuffled() {
        assert!(every_a_in_b(&[1, 2], &[5, 2, 3, 1, 7]));
    }

    #[test]
    fn test_not_every_a_in_b() {
        assert!(!every_a_in_b(&[1, 2, 3], &[2, 3]));
    }
}
