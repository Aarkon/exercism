use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub struct Clock(i32);

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let additional_hours = (minutes / 60) % 24;
        let hours = 24 + hours % 24 + additional_hours;
        let res = hours % 24 * 60 + minutes % 60;
        let res = if res < 0 { 60 * 24 + res } else { res };
        Clock(res)
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        Clock::new(self.hours(), self.minutes() + minutes)
    }

    fn hours(&self) -> i32 {
        self.0 / 60 % 24
    }

    fn minutes(&self) -> i32 {
        self.0 - ((self.0 / 60) * 60)
    }
}

impl Display for Clock {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}", pad(self.hours()), pad(self.minutes()))
    }
}

impl PartialEq<Self> for Clock {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

fn pad(n: i32) -> String {
    format!("{:0>2}", n)
}

#[cfg(test)]
mod unittests {
    use crate::{pad, Clock};

    #[test]
    fn test_minutes() {
        assert_eq!(Clock::new(2, 3).minutes(), 3);
        assert_eq!(Clock(123).minutes(), 3);
    }

    #[test]
    fn test_padding() {
        assert_eq!(pad(3), "03");
        assert_eq!(pad(9), "09");
        assert_eq!(pad(10), "10");
    }
}
