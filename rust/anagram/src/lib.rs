use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    possible_anagrams
        .iter()
        .filter(|&&candidate| is_anagram(word, candidate))
        .cloned()
        .collect::<HashSet<&str>>()
}

fn is_anagram(word: &str, candidate: &str) -> bool {
    if word.len() != candidate.len() {
        false
    } else {
        let word = word.to_lowercase();
        let candidate = candidate.to_lowercase();
        if word == candidate {
            false
        } else {
            let mut c = candidate.clone();
            for chr in word.chars() {
                c.find(|cc| cc == chr).map(|i| c.remove(i));
            }
            let mut d = word.clone();
            for chr in candidate.chars() {
                d.find(|cc| cc == chr).map(|i| d.remove(i));
            }
            c.is_empty() && d.is_empty()
        }
    }
}

#[cfg(test)]
mod anagram_unittests {
    use crate::is_anagram;

    #[test]
    fn test_anagram_detection() {
        assert_eq!(is_anagram("foo", "oof"), true);
        assert_eq!(is_anagram("foo", "oOf"), true);
        assert_eq!(is_anagram("foo", "foo"), false);
        assert_eq!(is_anagram("foo", "fOo"), false);
        assert_eq!(is_anagram("foo", "bar"), false);
    }
}
