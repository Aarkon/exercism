// This stub file contains items that aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

pub struct Player {
    pub health: u32,
    pub mana: Option<u32>,
    pub level: u32,
}

impl Player {
    pub fn revive(&self) -> Option<Player> {
        match (self.health, self.level >= 10, self.mana) {
            (0, false, _) => Some(Player { health: 100, mana: None, level: self.level }),
            (0, true, Some(0)) => Some(Player { health: 100, mana: Some(100), level: self.level }),
            _ => None
        }
    }

    pub fn cast_spell(&mut self, mana_cost: u32) -> u32 {
        match self.mana {
            None => {
                if self.health < mana_cost { self.health = 0; } else { self.health = self.health - mana_cost; }
                0
            }
            Some(m) => if m < mana_cost { 0 } else {
                self.mana = Some(m - mana_cost);
                mana_cost * 2
            }
        }
    }
}
