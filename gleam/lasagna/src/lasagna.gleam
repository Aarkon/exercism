pub fn expected_minutes_in_oven() -> Int {
  40
}

pub fn remaining_minutes_in_oven(already_spent: Int) -> Int {
  expected_minutes_in_oven() - already_spent
}

pub fn preparation_time_in_minutes(layers: Int) -> Int {
  layers * 2
}

pub fn total_time_in_minutes(layers: Int, time_in_oven: Int) -> Int {
  preparation_time_in_minutes(layers) + time_in_oven
}

pub fn alarm() -> String {
  "Ding!"
}
